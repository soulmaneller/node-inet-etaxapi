Library for inet e-taxinvoice with nodejs.

# How to use

```javascript
const fs = require( 'fs' );
const Inet = require( 'inet-etaxapi' );
const etax = new Inet( options );

etax.send( ServiceCode, data, opts ).then( result => {
    // Result from api
}).catch( err => {
    // Error
});
```

## Constructor Options

- **SellerTaxId** String *(Required)* : Seller tax ID
- **UserCode** String *(Required)* : User code from INET
- **AccessKey** String *(Required)* : Certificate access key
- **APIKey** String *(Required)* : Api key from INET
- **SellerBranchId** String *(Optional)* : Seller branch ID
- **baseURL** String *(Optional)* : INET E-Taxinvoice base url [default: `https://etaxinter.summitthai.com` ]
- **uri** String *(Optional)* : INET E-Taxinvoice uri [default: `/etaxdocumentws/etaxsigndocument`]

## Methods

### send( ServiceCode, data, options )

return Promise

**ServiceCode**: String

Must be: `S01`, `S02`, `S03`, `S04`, `S05`, `S06`, `S07`

**data**: Object

- **XMLContent** FileStream : XML content for `S01`, `S04`, `S07`
- **TextContent** FileStream : CSV content for `S02`, `S03`, `S05`, `S06`
- **PDFContent** FileStream : PDF content for `S04`, `S06`
- **EncryptPassword** String : Password for open PDF file

`XMLContent`, `TextContent`, `PDFContent` please use `fs.createReadStream( [filepath] )`
