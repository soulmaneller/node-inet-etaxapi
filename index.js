const request = require( 'request-promise' );

class EtaxAPI {
    constructor( obj ) {
        const requiredKeys = [ 'SellerTaxId', 'UserCode', 'AccessKey', 'APIKey' ];
        for( let key of requiredKeys ) {
            let data = obj[ key ];
            if( !data ) throw new Error( `Invalid key: ${ key }`)
        }

        this.SellerTaxId    = obj.SellerTaxId;
        this.SellerBranchId = obj.SellerBranchId;
        this.UserCode       = obj.UserCode;
        this.AccessKey      = obj.AccessKey;
        this.APIKey         = obj.APIKey;

        this.baseURL        = obj.baseURL || `https://etaxinter.summitthai.com`;
        this.uri            = obj.uri || `/etaxdocumentws/etaxsigndocument`;
    }

    send( ServiceCode, data, opts ) {
        const types = [ 'S01', 'S02', 'S03', 'S04', 'S05', 'S06', 'S07' ];
        if( types.indexOf( ServiceCode ) === -1 ) {
            return Promise.reject( `Invalid ServiceCode: ${ ServiceCode }` );
        }
        let reqObj = {};
        const dataTypes = {
            XMLContent  : [ 'S01', 'S04', 'S07' ],
            TextContent : [ 'S02', 'S03', 'S05', 'S06' ],
            PDFContent  : [ 'S04', 'S06' ],
        };

        for( let t in dataTypes ) {
            if( dataTypes[ t ].indexOf( ServiceCode ) !== -1 ) {
                if( !data.hasOwnProperty( t )) return Promise.reject( `ServiceCode: ${ ServiceCode } must has ${ t }`)
                reqObj[ t ] = data[ t ];
            }
        }

        if( data.hasOwnProperty( 'EncryptPassword' )) {
            reqObj.EncryptPassword = data.EncryptPassword;
        }

        const self = this;
        const { SellerTaxId, SellerBranchId, UserCode, AccessKey, APIKey } = self;

        const formData = {
            SellerTaxId,
            SellerBranchId,
            UserCode,
            AccessKey,
            APIKey,
            ServiceCode,
            ...reqObj
        }
        return request.post({
            url: self.baseURL + self.uri,
            formData,
            ...opts,
        });
    }
}

module.exports = EtaxAPI;
